<?
$APPLICATION->RestartBuffer();

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=vote.result.csv");
header("Pragma: no-cache");
header("Expires: 0");

$out = fopen('php://output', 'w');

foreach ($arResult['VOTE'] as $list) {
    fputcsv($out, Project\Tools\Utility\Content::toWin1251(array($list['DATE'] .'.')));
    foreach ($list['ITEM'] as $arItems) {
        ob_start();
        if ($arItems["POST_PROPERTIES"]['UF_IDEA_RESULT']['VALUE']) {
            $arPostField = $arItems["POST_PROPERTIES"]['UF_IDEA_RESULT'];
            $APPLICATION->IncludeComponent(
                    "bitrix:system.field.view", $arPostField["USER_TYPE"]["USER_TYPE_ID"], array("arUserField" => $arPostField), null, array("HIDE_ICONS" => "Y"));
            ?> (<?= implode('/', $arItems['RESULT']) ?>)<?
        }
        $status = trim(strip_tags(ob_get_clean()));

        fputcsv($out, Project\Tools\Utility\Content::toWin1251(array(
            $arItems['DATE'],
            $arItems['TITLE'],
            'https://portal.team-market.ru/company/personal/user/1/blog/' . $arItems['UF_POST_VOTE'] . '/',
            $status,
//            strip_tags(preg_replace('/\<br(\s*)?\/?\>/i', "\n", $arItems['DETAIL_TEXT'])),
        )));
    }
}
fclose($out);
exit;
