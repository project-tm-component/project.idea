<div class="idea-vote-result">
    <? foreach ($arResult['VOTE'] as $list) { ?>
        <div class="feed-item-wrap">
            <div class="feed-post-block">
                <div class="feed-post-cont-wrap sonet-log-item-createdby-1 sonet-log-item-where-U-1-all sonet-log-item-where-U-1-blog-post-vote sonet-log-item-where-U-1-blog" id="blg-post-img-804">
                    <div class="feed-post-title-block">
                        <a class="feed-post-user-name"><?= $list['DATE'] ?></a>
                        <? foreach ($list['ITEM'] as $arItems) { ?>
                            <div class="feed-post-item">
                                <a class="feed-post-title" href="/company/personal/user/1/blog/<?= $arItems['UF_POST_VOTE'] ?>/"><?= $arItems['TITLE'] ?></a>
                                <? if ($arItems["POST_PROPERTIES"]['UF_IDEA_RESULT']['VALUE']) { ?>
                                    <p class="status-color status-color-<?= $arItems["POST_PROPERTIES"]['UF_STATUS']['VALUE'] ?>"><?
                                        $arPostField = $arItems["POST_PROPERTIES"]['UF_IDEA_RESULT'];
                                        $APPLICATION->IncludeComponent(
                                                "bitrix:system.field.view", $arPostField["USER_TYPE"]["USER_TYPE_ID"], array("arUserField" => $arPostField), null, array("HIDE_ICONS" => "Y"));
                                        ?>
                                        (<?= implode('/', $arItems['RESULT']) ?>)
                                    </p>
                                <? } ?>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>

</div>