<?php

CBitrixComponent::includeComponentClass('project:component');

use Bitrix\Main\Type\DateTime;

class projectIdeaVoteResult extends projectComponent {

    public function getVote() {

    }

    public function executeComponent() {
        if ($this->projectCache()) {
            if (self::projectLoader('blog', 'vote', 'socialnetwork')) {

                $arFilter = array(
                    '!UF_POST_VOTE' => 0,
                    'PUBLISH_STATUS' => array(
                        0 => 'P',
                        1 => 'K',
                        2 => 'D',
                    ),
                    'BLOG_ID' => Project\Idea\Config::BLOG_ID,
                );
                $this->arResult['VOTE'] = array();
                $p = new blogTextParser(false, false, array("bPublic" => false));
                $dbPost = CBlogPost::GetList(array(), $arFilter, false, false, array('ID', 'TITLE', 'DETAIL_TEXT', 'UF_STATUS', 'UF_POST_VOTE'));
                while ($arPost = $dbPost->Fetch()) {
                    list($arPost['POST_PROPERTIES'], $arPostFieldsVote) = Project\Tools\Utility\Cache::get(array(__CLASS__, $arPost['ID']), function() use($arPost) {
                                global $USER_FIELD_MANAGER;
                                $arPostFieldsPost = $USER_FIELD_MANAGER->GetUserFields("BLOG_POST", $arPost["ID"], LANGUAGE_ID);
                                $arPostFieldsVote = $USER_FIELD_MANAGER->GetUserFields("BLOG_POST", $arPost["UF_POST_VOTE"], LANGUAGE_ID);
                                if ($arPostFieldsVote['UF_BLOG_POST_VOTE']) {
                                    $arPostFieldsVote['POST_ID'] = $arPostFieldsVote['ENTITY_VALUE_ID'];
                                }
                                return array($arPostFieldsPost, $arPostFieldsVote);
                            });

                    if ($arPostFieldsVote['UF_BLOG_POST_VOTE']['VALUE']) {
                        $arPostField = $arPostFieldsVote['UF_BLOG_POST_VOTE'];
                        $result = \Bitrix\Vote\Uf\Manager::getInstance($arPostField)->loadFromEntity()[$arPostField['VALUE']];
                        if ($result) {
                            if ($result['DATE_END']->getTimestamp() <= time()) {
                                $arPost['RESULT'] = array();
                                $arPost['VOTE'] = 0;
                                foreach ($result['QUESTIONS'] as $list) {
                                    foreach ($list['ANSWERS'] as $value) {
                                        $arPost['RESULT'][$value['MESSAGE']] = $value['COUNTER'];
                                        $arPost['VOTE'] = $arPost['VOTE'] + (Project\Idea\Config::VOTE_SETTING['ANSWERS'][0] == $value['MESSAGE'] ? 1 : -1) * $value['COUNTER'];
                                    }
                                }
                                $p->arUserfields = array("UF_BLOG_POST_FILE" => array_merge($arPostFieldsVote["UF_BLOG_POST_FILE"], array("TAG" => "DOCUMENT ID")));
                                $arPost["DATE_END"] = $result['DATE_END'];
                                $arPost["DETAIL_TEXT"] = $p->convert($arPost["DETAIL_TEXT"], false, false, array(
                                    'HTML' => 'N',
                                    'ANCHOR' => 'Y',
                                    'BIU' => 'Y',
                                    'IMG' => 'Y',
                                    'QUOTE' => 'Y',
                                    'CODE' => 'Y',
                                    'FONT' => 'Y',
                                    'LIST' => 'Y',
                                    'SMILES' => 'Y',
                                    'NL2BR' => 'N',
                                    'VIDEO' => 'Y',
                                    'USER' => 'Y',
                                    'TAG' => 'Y',
                                    'SHORT_ANCHOR' => 'Y',
                                        ), array(
                                    'imageWidth' => '800',
                                    'imageHeight' => '1000',
                                    'pathToUser' => '/company/personal/user/#user_id#/',
                                ));
                                ksort($arPost['RESULT']);
                                $this->arResult['VOTE'][date('Y-m', $arPost['DATE_END']->getTimestamp())]['DATE'] = CIBlockFormatProperties::DateFormat("f Y", $arPost['DATE_END']->getTimestamp(), CSite::GetDateFormat());
                                $arPost['DATE'] = CIBlockFormatProperties::DateFormat("j F Y", $arPost['DATE_END']->getTimestamp(), CSite::GetDateFormat());
                                $this->arResult['VOTE'][date('Y-m', $arPost['DATE_END']->getTimestamp())]['ITEM'][] = $arPost;
                            }
                        }
                    }
                }
                $this->projectTemplate();
            }
        }
    }

}
